#include "stdafx.h"
#include "windows.h"
#include <iostream>
#include <string>
using namespace std;

DCB dcb; 
HANDLE hCom; 
BOOL fSuccess;
char *pcCommPort = "COM6";


static int index = 0;  //la lb lc ld le lf lg
static int index2 = 2; //da db dc dd de
static int statuey = 0;
static int statuez = 0;
static int statuex = 0;
static int statuel = 0;

DWORD da = 0x00000000;
DWORD db = 0x44444444;
DWORD dc = 0x88888888;
DWORD dd = 0xcccccccc;
DWORD de = 0xffffffff;

string la[7];
//string la = "F:\\music\\a.wav"; // index ==1

 LPCWSTR stringToLPCWSTR(string str)
{
	size_t strSize = str.length() + 1;
    size_t convertedChars = 0;
	wchar_t *wcstring = (wchar_t *)malloc(sizeof(wchar_t)*(str.length()-1));
	mbstowcs_s(&convertedChars, wcstring, strSize, str.c_str(), _TRUNCATE);
	return wcstring;
}

   void SendCommand( const char *cmd )// Example: SendCommand("AT");
{
	DWORD written;
	char cr[1] = {0x0D};
	WriteFile(hCom,cmd,strlen(cmd),&written,NULL);
	WriteFile(hCom,cr,1,&written,NULL);
}

char *ReadResponse(void)			// Example: printf("Response: '%s'\n", ReadResponse() );
{
	DWORD read = 0;
	static char buf[1024]; //1024
	buf[0] = NULL; // safety termination
	BOOL b = ReadFile(hCom,buf,1024,&read,NULL);
	buf[read] = NULL;
	return &buf[0];
}

char *substr( const char* str, int start, int length ) // Example: "substr("ATtest", 2, 4) kommer ge str鋘gen 'test'
{
	static char buf[512]; //512
	int i = 0;
	for( i; i < length; i++)
		buf[i] = str[i+start];
	buf[length] = 0;
	return buf;
}
int main(int argc, char *argv[])
{

	la[0] = "F:\\music\\a.wav";
	la[1] = "F:\\music\\b.wav";
	la[2] = "F:\\music\\c.wav";
	la[3] = "F:\\music\\d.wav";
	la[4] = "F:\\music\\e.wav";
    la[5] = "F:\\music\\f.wav";
	la[6] = "F:\\music\\g.wav";
	PlaySound(stringToLPCWSTR(la[0]), NULL, SND_FILENAME | SND_ASYNC);
	hCom = CreateFileA( pcCommPort,
                    GENERIC_READ | GENERIC_WRITE,
                    0,    // must be opened with exclusive-access
                    NULL, // no security attributes
                    OPEN_EXISTING, // must use OPEN_EXISTING
                    0,    // not overlapped I/O
                    NULL  // hTemplate must be NULL for comm devices
                    );

   if (hCom == INVALID_HANDLE_VALUE)//INVALID_HANDLE_VALUE是在winbase.h中定义的 
   {
       // Handle the error.
       printf ("CreateFile failed with error*** %d.\n", GetLastError());
       printf ("1");
       system("pause");//“pause”这个系统命令的功能很简单，就是在命令行上输出一行类似于“Press   any   key   to   exit”的字，等待用户按一个键，然后返回
	   return (1);
   }

   // Build on the current configuration, and skip setting the size
   // of the input and output buffers with SetupComm.

   fSuccess = GetCommState(hCom, &dcb);

   if (!fSuccess) 
   {
      // Handle the error.
      printf ("GetCommState failed with error %d.\n", GetLastError());
      printf ("2");
      system("pause");
      return (2);
   }

   // Fill in DCB: 115200 bps, 8 data bits, no parity, and 1 stop bit.
   
   memset(&dcb, 0x00, sizeof(dcb) );  //Initialize handshake...

   dcb.fBinary = TRUE;  
   dcb.BaudRate = CBR_115200;     // set the baud rate
   dcb.ByteSize = 8;             // data size, xmit, and rcv
   dcb.Parity = NOPARITY;        // no parity bit
   dcb.StopBits = ONESTOPBIT;    // one stop bit

   fSuccess = SetCommState(hCom, &dcb);

   if (!fSuccess) 
   {
      // Handle the error.
      printf ("SetCommState failed with error %d.\n", GetLastError());
      printf ("3");
      system("pause");
      return (3);
   }

	COMMTIMEOUTS CommTimeouts;
	GetCommTimeouts (hCom, &CommTimeouts);

	// Change the COMMTIMEOUTS structure settings.
	CommTimeouts.ReadIntervalTimeout = 100;  
	CommTimeouts.ReadTotalTimeoutMultiplier = 0;  
	CommTimeouts.ReadTotalTimeoutConstant = 0;    
	CommTimeouts.WriteTotalTimeoutMultiplier = 10;  
	CommTimeouts.WriteTotalTimeoutConstant = 1000;    

	// Set the timeout parameters for all read and write operations
	// on the port. 
	if (!SetCommTimeouts (hCom, &CommTimeouts))
	{
	  return (4);
	}

   printf ("Serial port %s successfully reconfigured.\n", pcCommPort);
     

char *resulttt;  //sendcommand命令
char *resultg;

char *result;
char *res;
int res2;
int recordl = 2000;
char *resulttem; //temperature sensors
char *restem;
int res2tem;

char *resultx; //Accelerometer x sensors
char *resx;
int res2x;
int recordx=2000;

char *resulty; //Accelerometer y sensors
char *resy;
int res2y;
int recordy=2000; //记录上一次的y值

char *resultz; //Accelerometer z sensors
char *resz;
int res2z;
int recordz=2000; //记录上一次的z值

//SendCommand("AT");
printf("\nInactive");
while(1){
	SendCommand("ATs120=3"); //让GPI0工作 绿灯 表示在工作
	resultg = ReadResponse(); 
	SendCommand("ATs203?"); //Read the Light sensor
  result = ReadResponse();  
  //printf("@@@");
  //resulttem = Read
  res = substr(result, 2, 4);
  res2 = atoi(res);
  //strncpy(res,result,2);
  
  SendCommand("ATs204?"); // Read temperature
  resulttem = ReadResponse();
  restem = substr(resulttem, 2, 4); 
  res2tem = atoi(restem);
  ////////////
  SendCommand("ATs200?"); // Read X - Accelerometer
  resultx = ReadResponse();
  resx = substr(resultx, 2, 4);
  res2x = atoi(resx);

  SendCommand("ATs201?"); // Read Y - Accelerometer
  resulty = ReadResponse();
  resy = substr(resulty, 2, 4);
  res2y = atoi(resy);


  SendCommand("ATs202?"); // Read Z - Accelerometer
  resultz = ReadResponse();
  resz = substr(resultz, 2, 4);
  res2z = atoi(resz);
  /*printf("\n\nLight_Sensor: %d", res2);
  printf("\nTemperature_Sensor: %d",res2tem);
  printf("\nAccelerometer X:%d Y:%d Z:%d",res2x, res2y, res2z);*/
  //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
  if((res2>2000)&&((res2-recordl)>1000))
	  {
		  statuel++;
		  if(statuel%2==1)
		  printf("\nActive");
		  else printf("\nInactive");
       }
  //if(statuel%2==0)
  //{ SendCommand("ATs122=0"); //
  //  SendCommand("ATs126=3");
  //}
  if(statuel%2==1)
  {
   SendCommand("ATs122=3"); //设置2号口为output 绿灯
   SendCommand("ATs126=0");  //设置6号口为input 红灯

  if((recordy-res2y)>400) //accelerometer
  {   
	  index--; 
	  if(index>6)    
		  index = 0;
	  if(index<0)
		index=6;

	  if(index2>4)
		  index2 = 0;
	  if(index2<0)
		  index2 = 4;
      statuey = 1;
	  PlaySound(stringToLPCWSTR(la[index]), NULL, SND_FILENAME | SND_ASYNC);
	  printf("\nChanging song");
  }

  if(recordx>1850&&recordx<2150&&(recordx-res2x)>300)
  {   index2++;
      statuex = 1;
  }
  if(recordx>1850&&recordx<2150&&(res2x-recordx)>300)
  {   index2--;
      statuex = 1;
  }
  if(statuex == 1)
  {
	  if(index2==0)
		 waveOutSetVolume(0,da);
	else if(index2==1)
		waveOutSetVolume(0,db);
	else if(index2==2)
		waveOutSetVolume(0,dc);
	else if(index2==3)
		waveOutSetVolume(0,dd);
	else if(index2==4)
		waveOutSetVolume(0,de);
	printf("\nChanging volume");
  }

  statuey = 0;
  statuex = 0;

  recordy = res2y;
  recordz = res2z;
  recordx = res2x;
  recordl = res2;
  }

  else if(statuel%2==0)
  { 
	  SendCommand("ATs122=0");
	  SendCommand("ATs126=3");
  }

  resulttt = ReadResponse(); 
  SendCommand("ATs120=0");
  
  resultg = ReadResponse();
}

	return (0);
   
}


